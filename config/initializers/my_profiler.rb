require("#{Rails.root}/lib/my_profiler")
# in your test environment initializer file
def fetch_entries(_dir)
  Dir.entries("#{Rails.root}/app/models/").select { |entry| entry[-3..-1] == '.rb' }.map { |entry| entry[0..-4].camelize }
end
# return
return unless Rails.env.profiling?
require('active_support/inflector/inflections')
MODELS_TO_WATCH = fetch_entries('/app/models/') + fetch_entries('/app/controllers/') + fetch_entries('/app/services/')
TIME = Time.now

set_trace_func(proc { |event, _file, _line, id, _binding, classname|
  # if event == 'return' && file.to_s.include?('app/views/')
  #   puts classname.to_s
  #   puts id.to_s
  # end
  if event == 'return' && classname.to_s == 'ActionView::CompiledTemplates'
    MyProfiler.print_report(id.to_s)
  end

  if event == 'call' && MODELS_TO_WATCH.include?(classname.to_s)
    MyProfiler.m_hash(classname.to_s + '.' + id.to_s, 'call')
  end

  if event == 'return' && MODELS_TO_WATCH.include?(classname.to_s)
    MyProfiler.m_hash(classname.to_s + '.' + id.to_s, 'return')
  end
})
