Rails.application.routes.draw do
  root to: 'users#my_movie'
  # get '/users/my_movie' => 'users#my_movie', as: 'root'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get '/users/my_movie' => 'users#my_movie', as: 'my_movie'
  get '/users/my_tv' => 'users#my_tv', as: 'my_tv'
  resources :users
  # get '/users/new' => 'users#new'
  get '/recommends/rec_check' => 'recommends#rec_check'
  get '/whatsbetter/top_rated_movies' => 'whatsbetter#top_rated_movies', as: 'whatsbetter_top_rated_movies'
  resources :recommends
  get '/shows/imdb_cache' => 'shows#imdb_cache', as: 'imdb_cache'
  get '/shows/full_cast' => 'shows#full_cast', as: 'full_cast'
  get '/shows/show_why' => 'shows#show_why', as: 'show_why'
  get '/shows/user_mining' => 'shows#user_mining', as: 'user_mining'
  get '/shows/all_genres' => 'shows#all_genres', as: 'all_genres'
  get '/shows/genre' => 'shows#genre', as: 'genre'
  get '/shows/scrape' => 'shows#scrape', as: 'scrape'
  get '/shows/tv_tonight' => 'shows#tv_tonight', as: 'tv_tonight'
  get '/shows/dvd' => 'shows#dvd', as: 'dvd'
  get '/shows/imdb_search_title' => 'shows#imdb_search_title', as: 'imdb_search_title'
  resources :shows
  get '/whatsbetter/first_run' => 'whatsbetter#first_run', as: 'first_run'
  get '/whatsbetter/clear_skips' => 'whatsbetter#destroy_skips', as: 'clear_skips'
  get '/whatsbetter/rate_shows' => 'whatsbetter#rate_shows', as: 'rate_shows'
  get '/whatsbetter/top_rated' => 'whatsbetter#top_rated', as: 'whatsbetter_top_rated'
  get '/whatsbetter/rate_show' => 'whatsbetter#rate_show', as: 'rate_show'
  get '/whatsbetter/rate_group' => 'whatsbetter#rate_group', as: 'rate_group'
  get '/whatsbetter/rate_confusing' => 'whatsbetter#rate_confusing', as: 'rate_confusing'
  get '/whatsbetter/rate_show_recs' => 'whatsbetter#rate_show_recs', as: 'rate_show_recs'
  resources :whatsbetter
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
