class AddAltNameToShows < ActiveRecord::Migration[5.0]
  def change
     add_column :shows, :alt_name, :string
  end
end
