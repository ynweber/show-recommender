class ShowHasManyAttr < ActiveRecord::Migration[5.0]
  def change
    remove_column :shows, :actors
    add_column :shows, :actors, :text, array: true
    remove_column :shows, :creators
    add_column :shows, :creators, :text, array: true
    remove_column :shows, :alt_names
    add_column :shows, :alt_names, :text, array: true
  end
end
