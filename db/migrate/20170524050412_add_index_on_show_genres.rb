class AddIndexOnShowGenres < ActiveRecord::Migration[5.0]
  def change
    add_index :shows, :genres, using: 'gin'
  end
end
