class CreateRecommends < ActiveRecord::Migration[5.0]
  def change
    create_table :recommends do |t|
      t.text :first
      t.text :second

      t.timestamps
    end
  end
end
