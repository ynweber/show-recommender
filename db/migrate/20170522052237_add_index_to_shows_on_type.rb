class AddIndexToShowsOnType < ActiveRecord::Migration[5.0]
  def change
    add_index :shows, :show_type
  end
end
