class AddUniqueIndexes < ActiveRecord::Migration[5.0]
  def change
    add_index :recommends, [:first, :second], unique: true
    add_index :whatsbetters, [:first, :second], unique: true
  end
end
