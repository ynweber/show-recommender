class AddExternalLinksToShows < ActiveRecord::Migration[5.0]
  def change
    add_column :shows, :external_links, :json
  end
end
