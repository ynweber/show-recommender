class CreateScores < ActiveRecord::Migration[5.0]
  def change
    create_table :scores do |t|
      t.integer :user_id
      t.text :imdb
      t.text :skipped_against, array: true
      t.text :lost_against, array: true
      t.text :won_against, array: true
      t.integer :show_score
      t.integer :rec_score
      t.integer :rec_total

      t.timestamps
    end
  end
end
