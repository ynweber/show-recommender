class AddWritersDirectorsEtcToShows < ActiveRecord::Migration[5.0]
  def change
    # remove_column :shows, :creators
    add_column :shows, :directed_by, :text, array: true
    add_column :shows, :writing, :text, array: true
    add_column :shows, :cast, :text, array: true
    add_column :shows, :produced_by, :text, array: true
    add_column :shows, :animation_department, :text, array: true
    add_column :shows, :music_by, :text, array: true
    add_column :shows, :studio, :text, array: true
    add_column :shows, :certificate, :text, array: true
  end
end
