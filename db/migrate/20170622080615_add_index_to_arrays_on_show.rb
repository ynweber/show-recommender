class AddIndexToArraysOnShow < ActiveRecord::Migration[5.0]
  def change
    add_index :shows, :actors, using: 'gin'
    add_index :shows, :directed_by, using: 'gin'
    add_index :shows, :writing, using: 'gin'
    add_index :shows, :cast, using: 'gin'
    add_index :shows, :produced_by, using: 'gin'
    add_index :shows, :animation_department, using: 'gin'
    add_index :shows, :music_by, using: 'gin'
    add_index :shows, :studio, using: 'gin'
    add_index :shows, :certificate, using: 'gin'
  end
end
