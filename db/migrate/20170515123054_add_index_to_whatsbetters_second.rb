class AddIndexToWhatsbettersSecond < ActiveRecord::Migration[5.0]
  def change
    add_index :whatsbetters, [:second, :result]
  end
end
