class AddFavsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :favorite_movies, :text, array: true
    add_column :users, :favorite_movie_actors, :text, array: true
    add_column :users, :favorite_movie_genres, :text, array: true
    add_column :users, :favorite_movie_creators, :text, array: true
    add_column :users, :favorite_tv, :text, array: true
    add_column :users, :favorite_tv_actors, :text, array: true
    add_column :users, :favorite_tv_genres, :text, array: true
    add_column :users, :favorite_tv_creators, :text, array: true
    add_column :users, :recommended_movies, :text, array: true
    add_column :users, :recommended_tv, :text, array: true
  end
end
