class AddIndexOnScoresImdb < ActiveRecord::Migration[5.0]
  def change
    add_index :scores, :imdb
  end
end
