class CreateWhatsbetters < ActiveRecord::Migration[5.0]
  def change
    create_table :whatsbetters do |t|
      t.text :user_name
      t.text :first
      t.text :second
      t.integer :result

      t.timestamps
    end
  end
end
