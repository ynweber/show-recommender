class AddSubtextToShows < ActiveRecord::Migration[5.0]
  def change
    add_column :shows, :subtext, :string
  end
end
