class AddTvdbToShows < ActiveRecord::Migration[5.0]
  def change
    add_column :shows, :tvdb, :string
  end
end
