class UniqueImdbOnScores < ActiveRecord::Migration[5.0]
  def change
    remove_index(:whatsbetters, name: 'index_whatsbetters_on_first_and_second')
    add_index :whatsbetters, [:first, :second, :user_id], unique: true
    add_index :scores, [:user_id, :imdb], unique: true
  end
end
