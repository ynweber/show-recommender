class FixHavemanyGenres < ActiveRecord::Migration[5.0]
  def change
    remove_column :shows, :genres
    add_column :shows, :genres, :text, array: true
  end
end
