class AddIndexToWhatsbettersOnUpdatedAt < ActiveRecord::Migration[5.0]
  def change
    add_index(:whatsbetters, [:updated_at, :first], order: {updated_at: :desc})
    add_index(:whatsbetters, [:updated_at, :second], order: {updated_at: :desc})
  end
end
