class AddFavoritesToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :favorites, :text, array: true
  end
end
