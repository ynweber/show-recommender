class AddTypeToShows < ActiveRecord::Migration[5.0]
  def change
    add_column :shows, :type, :string
  end
end
