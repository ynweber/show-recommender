class FixShowsPeopleColumnNames < ActiveRecord::Migration[5.0]
  def change
    remove_column :shows, :cast
    rename_column :shows, :directed_by, :Directed
    rename_column :shows, :produced_by, :produced
    rename_column :shows, :music_by, :music
    rename_column :shows, :animation_department, :animation
  end
end
