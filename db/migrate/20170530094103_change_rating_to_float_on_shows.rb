class ChangeRatingToFloatOnShows < ActiveRecord::Migration[5.0]
  def change
      remove_column :shows, :rating
      add_column :shows, :rating, :float
  end
end
