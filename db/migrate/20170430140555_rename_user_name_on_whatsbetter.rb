class RenameUserNameOnWhatsbetter < ActiveRecord::Migration[5.0]
  def change
    rename_column :whatsbetters, :user_name, :user_id
  end
end
