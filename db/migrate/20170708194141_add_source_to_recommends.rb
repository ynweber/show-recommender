class AddSourceToRecommends < ActiveRecord::Migration[5.0]
  def change
    add_column :recommends, :source, :text
  end
end
