class AddCacheToShows < ActiveRecord::Migration[5.0]
  def change
    add_column :shows, :imdb_cache, :string
  end
end
