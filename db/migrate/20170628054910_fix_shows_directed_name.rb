class FixShowsDirectedName < ActiveRecord::Migration[5.0]
  def change
    rename_column :shows, :Directed, :directed
  end
end
