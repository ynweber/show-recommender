class AddIndexOnScoresImdbAndUser < ActiveRecord::Migration[5.0]
  def change
    add_index :scores, [:imdb, :user_id]
  end
end
