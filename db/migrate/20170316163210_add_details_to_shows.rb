class AddDetailsToShows < ActiveRecord::Migration[5.0]
  def change
    add_column :shows, :description, :string
    add_column :shows, :first_aired, :string
    add_column :shows, :genre, :string
    add_column :shows, :creator, :string
    add_column :shows, :actors, :string
    add_column :shows, :network, :string
    add_column :shows, :rating, :integer
    add_column :shows, :popluarity, :integer
    add_column :shows, :img_url, :string
  end
end
