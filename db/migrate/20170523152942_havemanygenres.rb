class Havemanygenres < ActiveRecord::Migration[5.0]
  def change
    remove_column :shows, :genres
    add_column :shows, :genres, :string, array: true
  end
end
