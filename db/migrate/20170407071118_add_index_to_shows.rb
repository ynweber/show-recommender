class AddIndexToShows < ActiveRecord::Migration[5.0]
  def change
    add_index :shows, :imdb
    add_index :recommends, :first
    add_index :recommends, :second
    add_index :whatsbetters, :first
  end
end
