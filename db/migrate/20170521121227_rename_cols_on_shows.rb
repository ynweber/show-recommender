class RenameColsOnShows < ActiveRecord::Migration[5.0]
  def change
    rename_column :shows, :creator, :creators
    rename_column :shows, :genre, :genres
    rename_column :shows, :alt_name, :alt_names
    rename_column :shows, :popluarity, :popularity
    rename_column :shows, :type, :show_type
  end
end
