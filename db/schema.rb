# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170719135832) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "recommends", force: :cascade do |t|
    t.text     "first"
    t.text     "second"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "source"
    t.index ["first", "second"], name: "index_recommends_on_first_and_second", unique: true, using: :btree
    t.index ["first"], name: "index_recommends_on_first", using: :btree
    t.index ["second"], name: "index_recommends_on_second", using: :btree
  end

  create_table "scores", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "imdb"
    t.text     "skipped_against",              array: true
    t.text     "lost_against",                 array: true
    t.text     "won_against",                  array: true
    t.integer  "show_score"
    t.integer  "rec_score"
    t.integer  "rec_total"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "show_total"
    t.index ["imdb", "user_id"], name: "index_scores_on_imdb_and_user_id", using: :btree
    t.index ["imdb"], name: "index_scores_on_imdb", using: :btree
    t.index ["user_id", "imdb"], name: "index_scores_on_user_id_and_imdb", unique: true, using: :btree
  end

  create_table "shows", force: :cascade do |t|
    t.text     "name"
    t.text     "imdb"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "description"
    t.string   "first_aired"
    t.string   "network"
    t.integer  "popularity"
    t.string   "img_url"
    t.string   "subtext"
    t.string   "imdb_cache"
    t.string   "show_type"
    t.text     "genres",                      array: true
    t.string   "tvdb"
    t.text     "actors",                      array: true
    t.text     "creators",                    array: true
    t.text     "alt_names",                   array: true
    t.float    "rating"
    t.text     "directed",                    array: true
    t.text     "writing",                     array: true
    t.text     "produced",                    array: true
    t.text     "animation",                   array: true
    t.text     "music",                       array: true
    t.text     "studio",                      array: true
    t.text     "certificate",                 array: true
    t.json     "external_links"
    t.index ["actors"], name: "index_shows_on_actors", using: :gin
    t.index ["animation"], name: "index_shows_on_animation", using: :gin
    t.index ["certificate"], name: "index_shows_on_certificate", using: :gin
    t.index ["directed"], name: "index_shows_on_directed", using: :gin
    t.index ["genres"], name: "index_shows_on_genres", using: :gin
    t.index ["imdb"], name: "index_shows_on_imdb", using: :btree
    t.index ["music"], name: "index_shows_on_music", using: :gin
    t.index ["produced"], name: "index_shows_on_produced", using: :gin
    t.index ["show_type"], name: "index_shows_on_show_type", using: :btree
    t.index ["studio"], name: "index_shows_on_studio", using: :gin
    t.index ["writing"], name: "index_shows_on_writing", using: :gin
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "password_digest"
    t.text     "favorite_movies",                      array: true
    t.text     "favorite_movie_actors",                array: true
    t.text     "favorite_movie_genres",                array: true
    t.text     "favorite_movie_creators",              array: true
    t.text     "favorite_tv",                          array: true
    t.text     "favorite_tv_actors",                   array: true
    t.text     "favorite_tv_genres",                   array: true
    t.text     "favorite_tv_creators",                 array: true
    t.text     "recommended_movies",                   array: true
    t.text     "recommended_tv",                       array: true
    t.text     "favorites",                            array: true
  end

  create_table "whatsbetters", force: :cascade do |t|
    t.text     "user_id"
    t.text     "first"
    t.text     "second"
    t.integer  "result"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["first", "result"], name: "index_whatsbetters_on_first_and_result", using: :btree
    t.index ["first", "second", "user_id"], name: "index_whatsbetters_on_first_and_second_and_user_id", unique: true, using: :btree
    t.index ["first"], name: "index_whatsbetters_on_first", using: :btree
    t.index ["second", "result"], name: "index_whatsbetters_on_second_and_result", using: :btree
    t.index ["updated_at", "first"], name: "index_whatsbetters_on_updated_at_and_first", order: { updated_at: :desc }, using: :btree
    t.index ["updated_at", "second"], name: "index_whatsbetters_on_updated_at_and_second", order: { updated_at: :desc }, using: :btree
  end

end
