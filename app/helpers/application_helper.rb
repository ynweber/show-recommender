# frozen_string_literal: true

# global helper
module ApplicationHelper
  def self.today_date(offset = nil)
    ApplicationRecord(offset)
  end

end
