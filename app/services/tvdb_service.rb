# frozen_string_literal: true

# scrap tvdb.com
class TvdbService
  def self.series_id(link)
    link.text.split('?')[-1].split('&')[1].split('=')[1]
  end

  def self.tvdb_link_parse(series_id)
    url = 'http://www.thetvdb.com/?tab=series&id=' + series_id
    showpage = UriService.new(url).parse
    imdb_line = showpage.css('#seriesform').css('input')[30]
    imdb_line ? imdb_line.attributes['value'].value : nil
  end

  def self.tvdbs
    tv_page = UriService.new('http://thetvdb.com/rss/newtoday.php').xmlparse
    tv_page.css('item').css('link').select(&:text)
           .map { |link| series_id(link) }
  end

  def self.new_today
    tvdbs.map { |tvdb| show_by_tvdb(tvdb) }.flatten
  end

  def self.show_by_tvdb(tvdb)
    show = Show.find_by(tvdb: tvdb)
    imdb = show ? show.imdb : tvdb_link_parse(tvdb)
    imdb ? add_show(imdb, tvdb) : nil
  end

  # def self.tvdb_imdb_parse(url)
  #   showpage = UriService.new(url).parse
  #   showpage.css('#seriesform').css('input')[30]
  #                  .attributes['value'].value
  # end

  def self.add_show(imdb, tvdb)
    show = Show.find_or_create_by(imdb: imdb)
    show.first_aired = Show.todays_date
    show.tvdb = tvdb
    show.save!
    show
  end

  # duplicated in application_record.
  def self.todays_date
    todays_date = Time.now.utc
    todays_date -= 25_200
    todays_date.strftime('%d %b %Y')
  end
end
