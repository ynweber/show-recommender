# frozen_string_literal: true

# match similar shows for rating
class MatchService
  def initialize(show, current_user = nil)
    @current_user = current_user
    if show.class == String
      @imdb = show
    else
      @show = show
      @imdb = @show.imdb
    end
  end

  def match
    shows =
      sort_by_genres(Show.shows_by_genre(@show.genres, @show.show_type))
    shows = match_popularity(shows)
    shows = match_cert(shows) if @show.certificate[0]
    Show.find_by(imdb: find_match(shows.map(&:imdb) - [@imdb])[0])
  end

  def find_match(imdbs)
    total_against = @current_user.score_cache(@imdb).total_against
    (imdbs - total_against - @current_user.score.skippable(imdbs))
      .reject { |imdb| imdb[2..3] == 'my' || imdb[0..1] != 'tt' }
  end

  # ! where rating == rating
  # def shows_by_genres
  #   shows = Show.shows_by_genre(@show.genres, @show.show_type)
  #   shows_sorted_by_genres(shows)
  #     .select { |show| show.popularity > @show.popularity / 10 }
  # end

  def match_popularity(shows)
    shows.select { |show| show.popularity > @show.popularity / 10 }
  end

  def match_cert(shows)
    # show_certificate = @show.certificate[0]
    # return shows unless show_certificate
    match_cert =
      shows.select do |show|
        cert = show[:certificate] || []
        cert[0] == @show.certificate[0]
      end
    match_cert.length > 50 ? match_cert : shows
  end

  def sort_by_genres(shows)
    shows.sort_by { |show| show.genres_comp_array(@show.genres) }.reverse
  end

  def already_matched?(_imdb_a, _imdb_b)
    to_return
  end
end
