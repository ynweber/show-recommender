# frozen_string_literal: true

module MyExtensions
  module Enumerable
    # like ||= for arrays. needs to be include in NilClass as well
    module ArrayOrEquals
      def a_or_equals
        return self if present?
        replace(yield)
        self
      end
    end
  end
end
