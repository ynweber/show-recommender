# frozen_string_literal: true

module MyExtensions
  module Enumerable
    # returns an array sorted by frequency of obj occurence
    module Frequency
      def sort_by_freq!
        each_with_object(Hash.new(0)) { |obj, hsh| hsh[obj] += 1 }
          .sort_by { |_obj, count| count }.map(&:first)
      end
    end
  end
end
