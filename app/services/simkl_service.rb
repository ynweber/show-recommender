# frozen_string_literal: true

class SimklService
  def initialize(imdb = nil, simkl = nil)
    @simkl = simkl
    imdb ||= parse_links['IMDB']
    @imdb = imdb
    return {} unless @imdb
    @show = Show.show_by_imdb(@imdb)
    ex_links = @show[:external_links]
    if ex_links
      @simkl ||= ex_links
    else
      @simkl ||= search
      links
    end
  end

  def self.tv_today(tv_date)
    page = UriService.new("https://simkl.com/tv/today/#{tv_date}/").parse
    shows = shows_by_simkl(
      page.css('.SimklTVPosterImgLargeHover').css('a')
          .map { |lnk| lnk['href'] }
    )
    shows.update_all(first_aired: tv_date)
    shows
  end

  def self.shows_by_simkl(simkls)
    # converts simkls array into something postgres understands
    f_shows = Show.shows_by_external_link('simkl', simkls)
    f_simkls = f_shows
        .pluck(:external_links)
        .map { |e_links| e_links[link_name] }

    f_shows + (simkls - f_simkls).map { |simkl| show_by_simkl(simkl) }.flatten
  end

  def self.show_by_simkl(simkl)
    SimklService.new(nil, simkl)
    Show.shows_by_external_link('simkl', simkl)
  end

  def page
    @page ||= UriService.new("https://simkl.com/#{@simkl}").parse if @simkl
  end

  def search
    response = UriService
               .new('https://simkl.com/ajax/search.php?s=' + @imdb).plain
    # binding.pry
    # re_hash = JSON.parse(response).values
    #   .select { |val| val.is_a?(Hash) }
    #   .values.find { |val| val.is_a?(Hash) }
    # re_hash.values[0]['url'] if re_hash
    url_hash = JSON.parse(response).flatten.find {|obj| obj.is_a?(Hash)}
    url = url_hash.values[0]["url"] if url_hash
    # binding.pry unless url
    url
  end

  # def find_imdb
  #   binding.pry
  #   parse_links['IMDB']
  # end

  def parse_links
    links =
      page.css('.SimklTVAboutTabsDetailsLinks').css('a')
          .map { |lnk| [lnk.text, lnk[:href]] }
    links + [['simkl', @simkl]]
    links.each_with_object({}) { |lnk_a, ex_hash| ex_hash[lnk_a[0]] = lnk_a[1] }
  end

  def links
    ex_hash = @show[:external_links].to_h
    return ex_hash unless @simkl && ex_hash.empty?
    ex_hash = parse_links
    @show.update_and_return!(
      external_links: ex_hash
    )
  end
end
