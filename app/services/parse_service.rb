# frozen_string_literal: true

# Parse Imdb Show Pages
class ParseService
  def initialize(parse_page, show)
    @parse_page = parse_page
    @show = show
  end

  def self.imdb_cache(show, imdb)
    url = 'http://www.imdb.com/title/' + imdb + '/'
    imdb_cache = UriService.new(url).plain
    show.update_and_return!(imdb_cache: imdb_cache)
  end

  def self.method_missing(method_name, *)
    'internet error' || super
  end

  def self.respond_to_missing?(method_name, *)
    true || super
  end

  def title
    to_return = @parse_page.css('.mini-article')
      .css('#ratingWidget').css('strong').text.strip
    if to_return.empty?
      to_return = @parse_page.css('.title_wrapper').css('h1').text.strip[0..-2]
    end
    binding.pry if to_return.empty?
    @show.update_and_return!(name: to_return)
  end

  def description
    to_return = @parse_page.css('.summary_text').text.strip
    @show.update!(description: to_return)
    to_return
  end

  def cert_line
    @parse_page.css('span')
                 .find { |span_tag| span_tag[:itemprop] == 'contentRating' }
  end

  def certificate
    cert, details = cert_line ? (cert_line.text.split(' for ')) : []
    details = details.to_s.split(', ').map { |detail| detail.split(' and ') }.flatten
    @show.update_and_return!(
      certificate: [cert.to_s.split('Rated ')[1]] + details
    )
  end

  def genres
    genres_ary =
      @parse_page.css('.canwrap').css('a')
                 .map do |target|
        target.text.strip if target['href'].include?('search/title?genres=')
      end
    puts 'washere'
    puts genres_ary
    @show.update_and_return!(genres: genres_ary.compact)
  end

  def actor_star_list
    @parse_page.css('span')
               .select { |span| span[:itemprop] == 'actors' }
               .map { |actor| actor.css('a')[0] }
  end

  def rating
    to_return = @parse_page.css('.ratingValue').css('strong')[0]
    to_return = to_return.text if to_return
    @show.update_and_return!(rating: to_return)
  end

  def img_url
    to_return = @parse_page.css('.poster').css('img')[0]
    return unless to_return
    to_return = to_return['src']
    @show.update!(img_url: to_return)
    to_return
  end

  def popularity
    to_return = @parse_page.css('#title-overview-widget')
                           .css('span.small').text.delete(',').to_i
    @show.update!(popularity: to_return)
    to_return
  end

  def subtext
    to_return = @parse_page.css('.subtext').text.delete("\n", '  ')
    @show.update!(subtext: to_return)
    to_return
  end

  def recommend
    rec_imdbs =
      @parse_page.css('.rec_poster_img')
                 .map do |img|
        img.parent['href'].split('/')[2]
        # Recommend.create_with(source: 'imdb')
        #   .find_or_create_by!(first: @show.imdb, second: rec_imdb)

        # Show.show_by_imdb(rec_imdb, {name: img['title']})
      end
    # shows = Show.shows_by_imdb(rec_imdbs)
    # current_show = Show.show_by_imdb(@show.imdb)
    # shows = shows.select { |show| current_show.relevant?(show) }
    # User.update_recs(shows, current_show, 'imdb')
    Recommend.create_recs(@show.imdb, rec_imdbs, 'imdb')
    # Show.shows_by_imdb(@show.recommendations.pluck(:second))
    @show.recommendations.pluck(:second)
  end

  def show_type
    subtext = @show.subtext
    to_return =
      if subtext.include?('TV') && !subtext.include?('Movie')
        'TV'
      else
        'MOVIE'
      end
    @show.update!(show_type: to_return)
    to_return
  end
end
