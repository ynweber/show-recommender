# frozen_string_literal: true

class MalService
  def initialize(show)
    @mal = show.external_links['MAL']
    @imdb = show.imdb
    @show = show
    fill_mal if @mal
  end

  def fill_mal
    genres
    Recommend.create_recs(@imdb, recommend, 'MAL')
  end

  def page
    @page ||= UriService.new(@mal).parse
  end

  def genres
    genres_ary = page.css('a')
                     .select { |lnk| lnk['href'].to_s.include?('genre') }
                     .map(&:text)
    genres_ary += ['Anime']
    @show.update_and_return!(genres: genres_ary)
  end

  def recommend
    mals =
      page.css('#anime_recommendation').css('a')
          .map { |lnk| lnk['href'].split('/')[-1].split('-') }.flatten.uniq
          .map { |mal| "http://myanimelist.net/anime/#{mal}/" }
          .reject { |mal| mal == @mal }
    Show.shows_by_external_link('MAL', mals).pluck(:imdb)
  end

  # def imdbs_by_mal(mals)
  #   # imdbs =
  #   #   mals.map do |mal|
  #   #     Show.where("external_links ->> 'MAL' = '#{mal}'").pluck(:imdb)
  #   #   end
  #   # imdbs.flatten
  # end
end
