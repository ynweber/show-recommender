# frozen_string_literal: true

# get the imdb pages
class ImdbService
  def self.parse_for_people_links(section)
    # people = section.next.next.css('a')
    # actors = people.css('.itemprop').map(&:parent)
    # people = section.next.next.css('.primary_photo')[0].css('a') # [0][:href]
    # people = section.next.next.css('a')
    people = section.next.next.css('a').select do |blah|
      blah['href'].include?('/name/nm') && blah.css('img').length > 0
    end
    people
    # actors.present? ? actors : people
  end

  def self.link_to_imdb(lin)
    lin['href'].split('/')[2].split('?')[0]
  end

  def self.link_to_name(lnk)
    name = lnk.text.strip
    name = nil if ['', ' '].include?(name)
    name ||= lnk.css('img')[0][:title]
    binding.pry if name == nil
    binding.pry if name == ''
    binding.pry if name == ' '

    name
  end

  # used by Show.person_create
  def self.person_link_to_img(lnk)
    img_line = lnk.parent.previous.previous
    img = img_line ? img_line.css('img')[0] : nil
    img ? img[:loadlate] : ''
  end

  def self.link_to_img(lin)
    lin.parent.parent.previous.previous.css('img')[0][:loadlate]
  end

  def self.link_to_subtext(lin)
    lin.next ? lin.next.next.text : ''
  end

  def self.link_to_show_type(lin)
    if lin.next
      lin.next.next.text.include?('–') ? 'TV' : 'MOVIE'
    else
      'TV'
    end
  end

  def self.search_page(qury, title_type)
    # qury = title[0..1] == 'nm' ? 'role=' + title : 'title=' + title
    url = 'http://www.imdb.com/search/title?count=2000&adult=include'
    # url + qury
    title_type = '&title_type=' + title_type
    UriService.new(url + title_type + qury).parse
  end

  def self.tv_today(start_date, end_date)
    title_type = 'tv_episode'
    # qury = 'title='+show_title
    qury = "&release_date=#{start_date},#{end_date}"
    parse_page = search_page(qury, title_type)
    lins = parse_page.css('.lister-item-header')
    lins = lins.map {|lin| lin.css('a')[0]}
    # lins = lins.css('a').select.with_index{|_,i| (i) % 2 == 0 }
    show_info_hash = parse_show_info(lins)
    Show.shows_by_imdb(show_info_hash.keys)
  end


  def self.search(title)
    title_type = 'feature,tv_movie,tv_series,mini_series,documentary'
    parse_page = search_page(
       (Show.person?(title) ?  "&role=" : "&title=") + title, title_type
    )
    lins = parse_page.css('.lister-item-header')
    lins = lins.map {|lin| lin.css('a')[0]}
    show_info_hash = parse_show_info(lins)
    # lins = parse_page.css('.lister-item-header').css('a')
    # show_info_hash = parse_show_info(lins)
    # !! not even sure this was working
    # show_info_hash.each { |imdb, info| Show.show_by_imdb(imdb, info) } if update
    Show.shows_sorted_by_imdb(show_info_hash.keys)
  end

  # def self.seach_and_update
  # end
  
  def self.parse_show_info(lins)
      lins.each_with_object({}) do |lin, info_hash|
      info_hash[link_to_imdb(lin)] = {
        img_url: link_to_img(lin) || '',
        name: link_to_name(lin),
        show_type: link_to_show_type(lin),
        subtext: link_to_subtext(lin)
      }
    end
  end


  def self.parse_show_info_old(parse_page)
    parse_page.css('.lister-item-header').css('a')
              .each_with_object({}) do |lin, info_hash|
      info_hash[link_to_imdb(lin)] = {
        img_url: link_to_img(lin) || '',
        name: link_to_name(lin),
        show_type: link_to_show_type(lin),
        subtext: link_to_subtext(lin)
      }
    end
  end

  def self.dvd
    dvd_page = UriService.new('http://www.imdb.com/sections/dvd/').parse
    imdbs =
      dvd_page.css('.list_item').css('.info').css('b').css('a')
              .map do |new_show|
        link_to_imdb(new_show)
      end
    Show.shows_by_imdb(imdbs)
  end

  def self.reviewers(imdb, page_number = 0)
    url = "https://www.imdb.com/title/#{imdb}/reviews?sort=userRating&dir=desc&ratingFilter=0"
    page = UriService.new(url).parse
    # page = UriService.new(
    #   "http://www.imdb.com/title/#{imdb}"\
    #   "/reviews-index?filter=love;start=#{page_number * 50}"
    # ).parse
    reviewer_list = reviewers_parse(page)

    # if page.css('img')
    #        .find { |img| img['src'] == 'http://i.media-imdb.com/f86.gif' }
    #   reviewer_list += reviewers(imdb, page_number + 1)
    # end

    reviewer_list
  end

  def self.reviewers_parse(page)
    # page.css('.comment-summary').css('a')
    #   .map { |a_tag| a_tag['href'] }.select {|href| href.include?('/user/') }
    #     .compact.uniq
    #     .map { |user| user.split('/')[2] }
    # binding.pry
    # page.css('a')
    #   .map { |a_tag| a_tag['href'] }.select {|href| href.include?('/user/') }
    #     .compact.uniq
    #     .map { |user| user.split('/')[2] }
    page.css('a').select {|ab| ab[:href].include?('/user/')}.compact.uniq.map {|ab| ab[:href].split('/')[2] }
  end

  def self.get_reviewer_page(url, page_number)
    page = UriService.new(url + ((page_number * 250) - 249).to_s).parse
    return nil if page.text.include?("list you're looking for is unavailable")
    page
  end

  def self.reviewer_page_count(page)
    page.css('.pagination').text.split(' of ')[1].split("\n")[0].to_i
  end

  def self.parse_reviewer_pages(url, page_number = 1)
    page = get_reviewer_page(url, page_number)
    return [] unless page
    trs = page.css('.lister-item') #[1..-1]

    if page.css('.pagination').css('a')[0] && reviewer_page_count(page) < 10
      trs += parse_reviewer_pages(url, page_number + 1)
    end

    trs
  end

  def self.reviewer_url(reviewer)
    imdb = reviewer.name.split('.')[0]
    "http://www.imdb.com/user/#{imdb}/ratings?view=compact&start="
  end

  # parse reviewers pages
  # build ratings
  # import ratings
  def self.build_reviewer(reviewer)
    # url = reviewer_url(reviewer)
    trs = parse_reviewer_pages(reviewer_url(reviewer))
    binding.pry if trs.nil?
    return unless trs && trs.any?
    ratings = build_ratings(trs)
    to_import = build_to_import(reviewer, ratings)
    # whats_import(to_import[:whats])
    scores_import(to_import[:scores])
  end

  def self.build_ratings(trs)
    ratings = trs.each_with_object({}) do |tr, hsh|
      imdb = tr.css('.lister-item-image')[0]['data-tconst']
      rating = tr.css('.ipl-rating-star__rating').text.to_i
      rating = nil if rating > 10
      hsh[imdb] = rating
    end
    ratings
  end

  def self.build_to_import(user, ratings)
    to_import = { 'scores': [], 'whats': [] }
    ratings.each do |s_imdb, rating|
      to_import[:scores].push(new_score(user, s_imdb, rating)) if rating
      # to_import[:whats] += build_whats(user, s_imdb, ratings, rating)
    end
    to_import
    # { 'whats': whats_ary, 'scores': score_ary }
  end

  # def self.build_whats(user, s_imdb, ratings, rating)
  #   (0..10).to_a.reject { |rat| rat == rating }
  #          .each_with_object([]) do |score, whats_ary|
  #     against = ratings.select { |_ky, vlu| vlu == score }.keys.sample
  #     next unless against
  #     if score > rating
  #       new_w = new_whats(user, against, s_imdb)
  #       # whats_ary.push(new_whats(user, against, s_imdb))
  #     else
  #       new_w = new_whats(user, s_imdb, against)
  #       #whats_ary.push(new_whats(user, s_imdb, against))
  #     end
  #     whats_ary.push(new_w)
  #   end
  # end

  def self.whats_import(whats_ary)
    Whatsbetter.import(
      whats_ary, validate: false, on_duplicate_key_ignore: true
    )
  end

  def self.scores_import(scores_ary)
    Score.import(scores_ary, validate: false, on_duplicate_key_ignore: true)
  end

  def self.new_score(user, imdb, rating)
    return unless rating
    Score.new(user: user, imdb: imdb, show_score: rating * 10)
  end

  # def self.new_whats(user, winner, loser)
  #   Whatsbetter.new(
  #     user: user,
  #     first: winner,
  #     second: loser,
  #     result: 1
  #   )
  # end
end
