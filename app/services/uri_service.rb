# frozen_string_literal: true

require('uri')

# gets and nokoparses urls
class UriService
  def initialize(url)
    @url = URI.escape(url)
    ApplicationRecord.errors("got #{@url}")
  end

  def plain
    response = HTTParty.get(@url)
    response.code == 200 ? response : nil
  rescue
    nil
  end

  def parse
    Nokogiri::HTML(plain)
  end

  def xmlparse
    Nokogiri::XML(plain)
  end
end
