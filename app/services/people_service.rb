# frozen_string_literal: true

# service for actors. hopefully works for creators etc as well
class PeopleService
  def initialize(show)
    @show = show
    @page = show.people_page

    fill_people unless @page == 'internet error'
    # after filling people, reset EVERYONES score for those people.
    # only necessary if show was scored before filling people
    # e.g. while rating shows, or hadnt yet filled full cast

    UserScore.clear_scores([show.imdb])
  end

  def fill_people
    @show.update!(
      actors: actors,
      writing: writing,
      produced: produced,
      animation: animation,
      music: music,
      directed: directed
    )
  end

  def actors
    stars =
      ParseService.new(@show.parse_page, @show).actor_star_list
                  .map { |lnk| ImdbService.link_to_imdb(lnk) }
    actors = (stars + fetch_people('Cast')).uniq
    actors || []
  end

  def writing
    fetch_people('Writing')
  end

  def directed
    fetch_people('Directed')
  end

  def produced
    fetch_people('Produced')
  end

  def animation
    fetch_people('Animation')
  end

  def music
    fetch_people('Music')
  end

  def people_section(person_type)
    @page.css('h4').find { |h4_tag| h4_tag.text.include?(person_type) }
  end

  def fetch_people(person_type)
    section = people_section(person_type)
    return [] unless section
    ImdbService.parse_for_people_links(section).map do |lnk|
      binding.pry if lnk.nil?
      person_type = 'Actors' if person_type == 'Cast'
      Show.person_create(lnk, person_type)
    end
  end
end
