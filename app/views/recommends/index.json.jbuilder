# frozen_string_literal: true

json.array!(@recommends, partial: 'recommends/recommend', as: :recommend)
