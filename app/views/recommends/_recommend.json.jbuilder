# frozen_string_literal: true

json.extract!(recommend, :id, :first, :second, :created_at, :updated_at)
json.url(recommend_url(recommend, format: :json))
