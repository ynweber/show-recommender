# frozen_string_literal: true

json.partial!('recommends/recommend', recommend: @recommend)
