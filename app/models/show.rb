# frozen_string_literal: true

# The Show Model
class Show < ApplicationRecord
  self.primary_key = 'imdb'
  # serialize :creators, Array
  # serialize :actors, Array
  # serialize :genres, Array
  # serialize :alt_names, Array
  validates :imdb, uniqueness: true, presence: true
  validate :imdb_valid
  # validates :imdb, length: { is: 9 }
  has_many :recommendations, class_name: 'Recommend', foreign_key: 'first'

  # dont think i use this anymore

  include ShowAttr

  def imdb_valid
    # !! newly added
    # dont know why this is necessary
    return false unless imdb
    imdb[0..1] == 'tt' || 'nm'
  end

  def self.shows_by_title(title)
    Show
      .where('name ilike :q', q: "%#{title}%")
      .order(popularity: :desc)
  end

  # called by: score.build_person, show_attr, shows_controller
  def self.shows_by_person(person, job = nil, limit = 3)
    # ImdbService.search(person) if update == true
    job ||= people_attrs.join("[0:#{limit}] || ")
    Show.where("? = ANY(#{job}[0:#{limit}])", person)
        .where("imdb not like 'ttmy%'")
  end

  # used by PeopleService
  def self.person_create(lnk, person_type)
    binding.pry if lnk.nil?
    imdb = ImdbService.link_to_imdb(lnk)
    img = ImdbService.person_link_to_img(lnk)
    # sometimes, esp directors, will be actors,
    # and a new img will be found for a preexisting person
    show = create_with(
      name: ImdbService.link_to_name(lnk),
      subtext: '',
      show_type: person_type,
      img_url: img
    ).find_or_create_by(imdb: imdb).person_img(img)
    show.imdb
  end

  # used by user_shows
  def self.people_by_job(job, imdbs)
    shows = select('unnest(' + job + '[0:3]) as person')
            .where(imdb: imdbs)
    people = shows.map(&:person)
    people.sort_by_freq![-(people.uniq.length / 10)..-1]
  end

  def self.favorite_attrs(imdbs, attr, s_range = (0..-1))
    attr =
      shows_by_imdb(imdbs).select { |show| show[attr] }
          .map { |show| show[attr][s_range] }.flatten.sort_by_freq!
    attr.length > 3 ? attr.compact[-3..-1].reverse : []
  end

  def self.favorite_cert(imdbs)
    [favorite_attrs(imdbs, 'certificate', (0..0))[0]] +
      favorite_attrs(imdbs, 'certificate', (1..-1))
  end

  def main_jobs
    jobs = %w[actors writing]
    jobs.push(show_type == 'MOVIE' ? 'directed' : 'produced')
  end

  # limit defaults to nil and not 0
  # in case i actually the first actors
  def people(jobs = main_jobs, limit = nil)
    jobs.each_with_object([]) { |ky, people| people += find_job(ky, limit) }
  end

  def find_job(ky, limit)
    ky_limit = limit || ky == 'actors' ? 2 : 0
    # ! to_a in case nil
    self[ky].to_a[0..ky_limit].map { |person| [person, '_', ky].join }
  end

  # people_attrs are the different types of jobs a person can have, e.g. actor, director, etc
  # this method should find all the people that fill those positions for this show.
  # !! GIVING UP ON GETTING THIS TO WORK
  def full_cast
    people_attrs.each_with_object([]) do |ky, cast|
      cast += set_job(ky)
    end
  end

  # as always, people are a kind of 'Show'
  # so this can read as get a sorted list of people by a job
  def set_job(ky)
    # job = Show.shows_sorted_by_imdb(self[ky])
    job = Show.shows_sorted_by_imdb([imdb])
    job.to_a.each { |person| person.show_type = ky }
    job
  end

  def person_img(img)
    update(img_url: img) unless self[:img_url] && self[:img_url] != ''
    self
  end

  # ! need to replace all show.find_by(imdb)
  # ! need to test imdbs for junk
  # !! need to reset people_vars when creating new show
  def self.show_by_imdb(imdb, show_info = {})
    show = find_by(imdb: imdb)
    unless show
      # !! newly added
      # needs fixing
      return nil unless imdb
      # show = create_with(show_info)
      show = find_or_create_by!(imdb: imdb)
      show.people_parser if person?(show.imdb)
    end

    set_show_info(show, show_info) if show_info

    show
  end

  def self.person_by_imdb(p_imdb, show)
    person = find_by(imdb: p_imdb)
    unless person || show.imdb[2..3] == 'my'
      # people_attrs.each { |ky| show[ky] = nil }
      # show.save
      show.people_parser
      person = find_by(imdb: p_imdb)
    end
    person || Show.create(imdb: p_imdb, img_url: '')
  end

  def self.shows_by_imdb(imdbs, show_info_hash = {})
    if imdbs.length > 100
      ApplicationRecord.errors(
        "big shows_by_imdb call #{imdbs.length} "
      )
    end
    (imdbs - where(imdb: imdbs).pluck(:imdb))
      .each { |imdb| show_by_imdb(imdb, show_info_hash[imdb]) }
    where(imdb: imdbs)
    # shows.map { |show| set_show_info(show, show_info_hash[show.imdb]) }
  end

  def self.set_show_info(show, show_info)
    show_info ||= {}
    show_info.each do |attr, val|
      show[attr] ||= val
    end
    show
  end

  # get a list of shows (or people) relevant to imdbs
  # and sort into a hash
  def self.shows_sorted_by_imdb(imdbs, show_info_hash = {})
    shows = shows_by_imdb(imdbs, show_info_hash)
    hash_object =
      shows.each_with_object({}) do |show, hash|
        hash[show.imdb] = show
      end
    imdbs.map { |index| hash_object[index] }
  end

  def self.shows_sorted_by_score(shows, current_user)
    # shows.to_a.sort_by(&:popularity)
    # shows.to_a.sort_by{|show|show.score(current_user.rec_score)}
    shows.sort_by do |show|
      [show.score(current_user)[:rec_score] || -1, show[:popularity].to_i || 0]
    end
  end

  # used by match_service
  def self.shows_by_genre(genres, show_type = %w[MOVIE TV], popularity = 0)
    # select(:imdb, :show_type, :genres, :certificate, :popularity)
    where('ARRAY[?] && genres', genres)
      .where(show_type: show_type)
      .where("popularity > #{popularity}")
      .where("imdb not like 'ttmy%'")
  end

  
  def self.shows_by_external_link(link_name, link_ids)
    qury = link_ids.to_s.tr('"', "'").delete('[').delete(']')
    Show.where("external_links ->> '#{link_name}' IN (#{qury})")
        # .pluck(:external_links)
        # .map { |e_links| e_links[link_name] }
  end

  def self.tv_tonight(start_date, end_date)
    # tvshows = Show.where(first_aired: todays_date)
    tvshows = ImdbService.tv_today(start_date, end_date) # if tvshows.empty?
    # tvshows = TvdbService.new_today if tvshows.empty?
    # tvshows = SimklService.tv_today(todays_date) if tvshows.empty?
    tvshows.to_a.compact
  end

  # used by user
  def self.favorite_actors(favs)
    # cant use pluck with the select [0:5]. cant figure another way
    actors = select('actors[0:2]').where(imdb: favs).map(&:actors).flatten
    actors.sort_by_freq![-(actors.uniq.length / 10)..-1]
  end

  def self.fill_people(imdbs)
    where(actors: nil).where(imdb: imdbs).find_each do |show|
      PeopleService.new(show)
    end
  end
  def relevant?(r_show)
    show_type == r_show.show_type &&
      r_show.popularity * 5 > popularity &&
      r_show.certificate[0] == certificate[0]
  end

  def score(current_user = nil)
    if show_type == 'internet_error'
      ApplicationRecord.errors.push(
        "score.show_type == 'internet error': #{caller[1]} "
      )
    end
    @score ||= current_user.score_cache(imdb, show_type)
  end

  def genres_comp_array(oshow_genres)
    cnt = (genres & oshow_genres).count
    [
      cnt,
      (genres.count - cnt).abs * -1,
      popularity
    ]
  end
end
