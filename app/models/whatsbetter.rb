# frozen_string_literal: true

# Whatsbetter rating methods
class Whatsbetter < ApplicationRecord
  belongs_to :user
  validates :first, uniqueness: { scope: %i[second user_id] }
  validates :first, presence: true
  validates :second, presence: true

  # validate :validation

  # def validation
  #   errors.add(first, 'repeat') if whats_validation(first, second)
  # end

  # def whats_validation(first, second)
  #   w_at = Whatsbetter.arel_table
  #   return true if where(w_at[:first].eq(first))
  #     .and(where(w_at[:second].eq(second)))[0]

  #   return true if where(w_at[:first].eq(second))
  #     .and(where(w_at[:second].eq(first)))[0]
  # end
  # belongs_to :score

  # def self.cached
  #   @cached ||= Hash.new { |hsh, ky| hsh[ky] = {} }
  # end

  # def self.reset_imdb(imdb)
  #   cached[imdb] = {}
  # end

  def self.destroy_whats(user_id, first, second)
    where(user: user_id, first: first, second: second).delete_all
    where(user: user_id, first: second, second: first).delete_all
    UserScore.clear_scores([first, second], user_id)
  end

  def self.w_at
    arel_table
  end

  def self.wins(imdb)
    ratings(imdb).select do |rating|
      imdb == rating.first && rating.result == 1
    end
  end

  def self.losses(imdb)
    ratings(imdb).select do |rating|
      imdb == rating.second && rating.result == 1
    end
  end

  def self.ratings(imdbs)
    imdbs = [imdbs] if imdbs.class == String
    imdbs =
      imdbs.reduce([]) do |total, imdb|
        total.push(ratings_find(imdb))
      end
    imdbs.flatten
  end

  def self.ratings_find(imdb)
    where(w_at[:first].eq(imdb))
      .or(where(w_at[:second].eq(imdb)))
  end

  def self.newest(imdb)
    ratings_find(imdb)
      .order(updated_at: :desc)
      .limit(1).pluck(:updated_at)[0]
  end

  def self.skips(imdb)
    ratings(imdb).select { |rating| rating.result.zero? }
  end

  def self.last_match
    Show.shows_by_imdb([last.first, last.second])
  end
end
