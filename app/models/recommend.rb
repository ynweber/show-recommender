# frozen_string_literal: true

# Recommend Table Methods
class Recommend < ApplicationRecord
  validates :first, presence: true
  validates :second, presence: true
  validates :first, uniqueness: { scope: [:second] }
  validate :first_not_second
  # belongs_to :show, foreign_key: 'first'

  def first_not_second
    errors.add(:first, 'cant equal second') if first == second
  end

  def self.rec_check(score)
    recs = Recommend.where(first: score.imdb).pluck(:second)
    recs.each do |rec|
      rec_scores = Score.where(imdb: rec)
      bad = rec_scores.where('show_score < 40')
      # delete_rec(rec, score) if rec_scores.count > 50 &&
      delete_rec(rec, score) if rec_scores.count > 10 &&
                                bad.count > rec_scores.count / 2
    end
  end

  def self.delete_rec(rec, score)
    ApplicationRecord.errors.push("deleted rec! #{rec} from #{score.imdb} ")
    Recommend.find_by(first: score.imdb, second: rec).delete

    Score.clear_score(nil, Show.show_by_imdb(rec))
  end

  def self.recs_check
    scores = Score.select('count(*), imdb').where('show_score > 80')
                  .group(:imdb).order(count: :desc)
    scores = scores.select { |score| score.count > 10 }
    scores.each { |score| rec_check(score) }
  end

  # used by User
  def self.group_recommend(imdbs)
    (Recommend.where(first: imdbs).pluck(:second).compact.uniq +
     Recommend.where(second: imdbs).pluck(:first).compact.uniq).uniq
  end

  def self.clean_recs(show)
    shows = Show.shows_by_imdb(show.recommendations.pluck(:second))
    shows = MatchService.new(show).sort_by_genres(shows)[15..-1] if shows
    Recommend.where(first: show.imdb, second: shows).delete_all if shows
  end

  def self.create_recs(imdb, imdbs, source)
    # ApplicationRecord
    #   .errors("Found #{shows[0..11].count} Recs for #{current_show.imdb}")
    shows = Show.shows_by_imdb(imdbs)
    c_show = Show.show_by_imdb(imdb)
    shows = shows.select { |show| c_show.relevant?(show) }
    shows.map do |second|
      next if second.imdb == imdb
      Recommend.create_with(source: source)
               .find_or_create_by(first: imdb, second: second.imdb)
    end
  end
end
