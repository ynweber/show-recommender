# frozen_string_literal: true

# global methods
class ApplicationRecord < ActiveRecord::Base
  include SessionsHelper

  self.abstract_class = true

  def self.errors(msg = nil)
    @errors ||= []
    @errors.push(msg + caller[1..2].to_s) if msg
    @errors
  end

  def self.errors_show
    errors = self.errors
    @errors = []
    errors
  end

  # def self.save_file(f_name, d_name, url)
  #   if File.file?(d_name + '/' + f_name)
  #     page = File.open(d_name + '/' + f_name, 'r')
  #     parse_page = Nokogiri::HTML(page)
  #     puts 'Have File'
  #   else
  #     parse_page = UriService(url).parse
  #     # parse_page = noko_url url
  #     File.open(d_name + '/' + f_name, 'w') do |file|
  #       file.write(page.force_encoding('UTF-8'))
  #     end
  #   end
  #   parse_page
  # end

  def current_user
    @current_user ||= User.find_by(id: session[:user])
  end

  # def self.date_string
  #   Time.now.strftime('%Y-%m-%d')
  # end

  def self.tvdb_date
    todays_date = Time.now.utc
    todays_date -= 25_200
    todays_date.strftime('%d %b %Y')
  end

  def self.todays_date(offset = 0)
    # todays_date = Time.now.utc
    # todays_date -= 25_200
    todays_date = Time.now.utc + (offset*24*60*60)
    # todays_date.strftime('%d-%m-%Y')
    todays_date.strftime('%Y-%m-%d')
  end

  def update_and_return(args)
    update(args)
    args.values[0]
  end

  def update_and_return!(args)
    update!(args)
    args.values[0]
  end

  def tt_validation
    %w[tt nm].include?(imdb[0..1])
  end

  def self.show?(imdb)
    imdb[0..1] == 'tt'
  end

  def self.person?(imdb)
    imdb[0..1] == 'nm'
  end

  def self.people_attrs
    %w[actors directed writing produced animation music]
  end

  def people_attrs
    %w[actors directed writing produced animation music]
  end
end
