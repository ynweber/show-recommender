# frozen_string_literal: true

# mixin defining methods to set attributes for Show model
module ShowAttr
  def imdb_cache
    self[:imdb_cache] ||= ParseService.imdb_cache(self, imdb)
  end

  def parse_imdb_cache
    imdb_cache ? Nokogiri::HTML(imdb_cache) : nil
  end

  # simklservice broken needs fixing !!!!!
  def external_links
    # self[:external_links] || SimklService.new(imdb).links
    self[:external_links] || {}
  end

  def mal
    MalService.new(self)
  end

  # i think this gets too huge, when loading lots of new shows.
  def parse_page
    @parse_page =
      if imdb[0..1] == 'tt'
        parse_imdb_cache
      else
        # shouldnt be here
        # unless i manually broke db

        rebuild_person
      end
  end

  def rebuild_person
    show = Show.shows_by_person(imdb, nil, 100)[0]
    return nil unless show
    # people_attrs.each { |ky| show[ky] = nil }
    # show.save
    Show.find_by(imdb: imdb).delete
    PeopleService.new(show)
    nil
  end

  def parser
    @parser ||=
      parse_page ? ParseService.new(parse_page, self) : ParseService
  end

  def people_page
    unless imdb[2..3] == 'my'
      @people_page ||=
        UriService.new(
          'http://www.imdb.com/title/' + imdb + '/fullcredits'
        ).parse
    end
    @people_page ||= 'internet error'
  end

  def people_parser
    @people_parse ||= PeopleService.new(self)
  end

  def description
    self[:description] ||= parser.description
  end

  def name
    self[:name] ||
      begin
      # if imdb[0..1] == 'nm'
      #   # people_attrs.each do |job|
      #   #   self[job] = nil
      #   # end
      #   Show.shows_by_person(imdb).delete_all
      #   people_parser
      #   self[:name]
      # else
      parser.title
      # end
    end
  end

  def certificate
    self[:certificate] || parser.certificate
  end

  def genres
    g_ary = self[:genres]
    if g_ary && g_ary.any? 
      g_ary 
    else
      external_links.key?('MAL') ? mal.genres : parser.genres
    end
  end

  def writing
    g_ary = self[:writing]
    g_ary ? g_ary : people_parser.writing
  end

  def directed
    c_ary = self[:directed]
    c_ary ? c_ary : people_parser.directed
  end

  def produced
    c_ary = self[:produced]
    c_ary ? c_ary : people_parser.produced
  end

  def animation
    c_ary = self[:animation]
    c_ary ? c_ary : people_parser.animation
  end

  def actors
    a_ary = self[:actors]
    a_ary ? a_ary : people_parser.actors
  end

  def music
    a_ary = self[:music]
    a_ary ? a_ary : people_parser.music
  end

  def rating
    self[:rating] ||= parser.rating
  end

  def img_url
    self[:img_url] ||
      begin
      imdb[0..1] == 'tt' ? parser.img_url : ''
    end
  end

  def popularity
    self[:popularity] ||= parser.popularity
  end

  def subtext
    self[:subtext] ||= parser.subtext
  end

  def show_type
    self[:show_type] || parser.show_type
  end

  def recommend
    @recs ||=
      begin
        recs = recommendations.pluck(:second)
        # if update
        recs = parser.recommend if recs.empty?
        #   expand_recs if recs.length < 12
        # end
        # # recs += back_recommend
        # recs = recommendations.pluck(:second)
        recs || []
      end
  end

  def expand_recs
    parser.recommend
    Recommend.create_recs(imdb, User.user_mining(self), 'users')
    Recommend.create_recs(imdb, User.same_people(self), 'people')
    Recommend.create_recs(imdb, back_recommend, 'back')
    Recommend.clean_recs(self)
  end

  def back_recommend
    Recommend.where(second: imdb).where("first not like 'ttmy%'")
             .pluck(:first).compact
  end

  def recommend_no_update
    recommendations.pluck(:second) || []
  end
end
