# frozen_string_literal: true

# creates my_movie & my_tv
module UserShows
  def my_movie
    favorites_clear if favorites_check
    favorites_by_type('MOVIE')
    movie = Show.find_by(imdb: 'ttmy_MOVIE' + id.to_s) || build_movie

    movie.score(self).update(
      lost_against: favorite_movies[0..19],
      won_against: favorite_movies[-20..-1],
      show_score: 100,
      rec_score: 100
    )
    movie
  end

  def my_tv
    favorites_clear if favorites_check
    favorites_by_type('TV')
    tv = Show.find_by(imdb: 'ttmy_TV' + id.to_s) || build_tv

    tv.score(self).update(
      lost_against: favorite_tv[0..19],
      won_against: favorite_tv[-20..-1],
      show_score: 100,
      rec_score: 100
    )
    tv
  end

  def build_movie
    Show.create(
      [
        base_hash('MOVIE'),
        people_hash(favorite_movies),
        movie_recs_hash,
        misc_hash(favorite_movies[0..50])
      ].reduce(&:merge)
    )
  end

  def build_tv
    Show.create(
      [
        base_hash('TV'),
        people_hash(favorite_tv),
        tv_recs_hash,
        misc_hash(favorite_tv[0..50])
      ].reduce(&:merge)
    )
  end

  def base_hash(show_type)
    {
      imdb_cache: '', imdb: "ttmy_#{show_type}#{id}", name: "My #{show_type}",
      rating:  10, popularity: 1_000_000,
      description: 'An imaginary show based on your results',
      subtext: '',
      show_type: show_type
    }
  end

  def people_hash(shows)
    {
      actors: favorite_people(shows, 'actors')[0..50],
      writing: favorite_people(shows, 'writing')[0..3],
      produced: favorite_people(shows, 'produced')[0..3],
      directed: favorite_people(shows, 'directed')[0..3],
      animation: favorite_people(shows, 'animation')[0..3],
      music: favorite_people(shows, 'music')[0..3]
    }
  end

  def misc_hash(imdbs)
    {
      genres: Show.favorite_attrs(imdbs, 'genres'),
      certificate: Show.favorite_cert(imdbs)
    }
  end

  def movie_recs_hash
    {
      recommendations: recommended('MOVIE')[0..25].map do |rec_imdb|
        Recommend.new(first: "ttmy_MOVIE#{id}", second: rec_imdb)
      end
    }
  end

  def tv_recs_hash
    {
      recommendations: recommended('TV')[0..25].map do |rec_imdb|
        Recommend.new(first: "ttmy_TV#{id}", second: rec_imdb)
      end
    }
  end

  def favorites_by_type(show_type)
    # !! this seems too heavy
    imdbs = favorites & Show.where(show_type: show_type).pluck(:imdb)
    if show_type == 'MOVIE'
      update_and_return!(favorite_movies: imdbs)
    else
      update_and_return!(favorite_tv: imdbs)
    end
    imdbs
  end



  # ! mb move this to show?
  def favorite_people(imdbs, job)
    people = Show.people_by_job(job, imdbs)
    p_imdbs = people.map { |person| person + '_' + job }
    #scores_cache(p_imdbs).sort_by(&:show_score).reverse
    scores_cache(p_imdbs).sort_by { |p_score| p_score.won_against.count }.reverse
      .map { |s_score| s_score.imdb.split('_')[0] }
  end

  def recommended(show_type)
    r_ms = recommended_imdbs & Show.where(show_type: show_type).pluck(:imdb)
    Show.where(actors: nil).where(imdb: r_ms[0..30]).find_each do |show|
      PeopleService.new(show)
    end
    r_ms
  end
end
