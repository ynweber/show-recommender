# frozen_string_literal: true

# Calcs & Keeps user score info.
class Score < ApplicationRecord
  Array.include(MyExtensions::Enumerable::ArrayOrEquals)
  # has_many :whatsbetter
  belongs_to :user
  has_many :whatsbetters, through: :user
  validate :validation
  # validates :imdb, format: { with: /^tt/ }

  def validation
    errors.add(imdb, imdb + ' not a tt') unless tt_validation
  end

  def show
    @show ||= Show.show_by_imdb(imdb)
  end

  def self.clear_score(user_id, show)
    reset_recs(show, user_id) if show?(show.imdb)
    imdbs = show.people
    where(w_user_id(user_id)).where(imdb: imdbs).delete_all unless imdbs.empty?
    # update_all does not update time stamps
    where(w_user_id(user_id)).where(imdb: show.imdb).update_all(
      show_total: nil, show_score: nil, skipped_against: nil, won_against: nil,
      lost_against: nil
    )
  end

  def fill_score
    show_total
    show_score
    rec_total
    rec_score
  end

  def self.w_user_id(user_id)
    user_id ? arel_table[:user_id].eq(user_id) : arel_table[:user_id].gt(-1)
  end

  def self.reset_recs(show, user_id = nil)
    where(w_user_id(user_id)).where(imdb: show.back_recommend)
                             .update_all(rec_total: nil, rec_score: nil)
  end

  def self.score_create(imdb, job = nil, to_update = true)
    s_score = new(imdb: imdb)
    s_score.build_person(job) if imdb[0..1] == 'nm' && to_update

    # !! make sure to update when i want
    s_score[:show_score] = '?'
    s_score[:rec_score] = -1
    s_score
  end

  def build_person(job = nil)
    scores = user.scores_cache(Show.shows_by_person(imdb.split('_')[0], job)
      .pluck(:imdb))
    update(
      won_against: scores.map(&:won_against).flatten,
      lost_against: scores.map(&:lost_against).flatten,
      rec_score: -1
    )
  end

  def skipped_against
    self[:skipped_against] ||
      update_and_return(
        skipped_against:
        (
          user.whatsbetter.skips(imdb).pluck(:first, :second).flatten - [imdb]
        ) || []
      )
  end

  def lost_against
    self[:lost_against] ||
      update_and_return!(
        lost_against: user.whatsbetter.losses(imdb).pluck(:first) || []
      )
  end

  def won_against
    self[:won_against] ||
      update_and_return!(
        won_against: user.whatsbetter.wins(imdb).pluck(:second) || []
      )
  end

  def show_total
    self[:show_total] ||
      update_and_return!(show_total: (won_against + lost_against).count)
  end

  def show_score
    self[:show_score] || update_and_return!(show_score: show_score_calc)
  end

  def show_score_calc
    wins = total_wins.count
    return 0 if wins.zero?
    losses = total_losses.count
    return 100 if losses.zero?
    wins / Float(wins + losses) * 100
  end

  def total_wins
    (user.scores_cache(won_against).map(&:won_against).flatten + won_against)
  end

  def total_losses
    (user.scores_cache(lost_against).map(&:lost_against).flatten + lost_against)
  end

  # assumes show_total
  def rec_total
    self[:rec_total] ||
      update_and_return(
        rec_total:
        # user.score.select('sum(show_total)').where(imdb: show.recommend)[0]
        user.score.select('count(*)').where(imdb: show.recommend)
      .where('show_total > 5')[0].count || 0
        # .sum || 0
      )
  end

  # requires show_score & show_total to already be set
  def rec_score(update = true)
    to_return = self[:rec_score]
    if update == true && to_return == -1 || to_return.nil?
      rec_scores =
        user.scores_cache(show.recommend).select(&:good_score?)
      update_and_return(rec_score: user.avg_score(rec_scores.map(&:imdb)))
    else
      to_return
    end
  end

  def good_score?
    already_scored? # &&
     # (self[:show_score].to_i - self[:rec_score].to_i).abs < 50
  end

  # !! Attempted a rec_score_calc like criticker
  # def rec_score_calc
  #   users = Score.where(imdb: imdb).where('show_score is not null')
  #     .where("user_id != #{user.id}").pluck(:user_id)
  #   me = user.score.where('show_score is not null').pluck(:imdb)
  #   diffs =
  #     users.map do |user_id|
  #       user_imdbs = Score.where(user_id: user_id)
  #         .where('show_score is not null').pluck(:imdb)
  #       matching_imdbs = user_imdbs & me
  #       diff_total =
  #         matching_imdbs.reduce(0) do |total, t_imdb|
  #           diff = user.score.find_by(imdb: t_imdb).show_score -
  #             Score.where(user_id: user_id).find_by(imdb: t_imdb).show_score
  #           total += diff.abs
  #         end
  #       [diff_total / matching_imdbs.length, user_id]
  #     end
  #   user_ids = diffs.sort[0..10].map { |diff| diff[1] }
  #   score = Score.select('sum(show_score) / count(show_score) as score')
  #                .where(user_id: user_ids)
  #                .where(imdb: imdb)[0].score

  #   diffs
  # end

  def self.skippable(imdbs)
    where('cardinality(skipped_against) > 5')
    .where(imdb: imdbs)
    .where.not('show_total > 5').pluck(:imdb)
  end

  def already_scored?(updating = true)
    total =
      if updating
        show_total
      else
        (self[:won_against].to_a + self[:lost_against].to_a).count
      end
    total > 5
  end

  # used by match service
  def total_against
    lost_against + won_against + skipped_against
  end

  def confusing
    (total_wins & total_losses)
  end

  def confusing?
    confusing.any?
  end

  def self.suprising_rec_scores
    rows = select('imdb, @ (show_score - rec_score) as diff')
           .where("imdb like 'tt%'")
           .where('rec_score > 0')
           .where('rec_total > 2').where('show_total > 5').order('diff desc')

    total = rows.map(&:diff).compact.reduce(0) { |sum, diff| sum + diff }
    [total, rows.map(&:imdb)]
  end

end
