# frozen_string_literal: true

# ! make this a service
# mixin for User Model. functions that refer directly to score.
module UserScore
  # sometimes this is called for specific user, e.g. whatsbetter.create
  # sometimes this is called for ALL users, e.g. PeopleService.fill_people
  def self.clear_scores(imdbs, user_id = nil)
    Show.shows_by_imdb(imdbs).each do |show|
      Score.clear_score(user_id, show)
    end
  end

  def score_cache(imdb, job = nil)
    job = 'actors' if job == 'cast'
    imdb = imdb + '_' + job if imdb[0..1] == 'nm'
    s_score = score.find_by(imdb: imdb)
    # s_score = s_score.test_update if s_score
    s_score || score.score_create(imdb, job)
  end

  # def check_scores(imdbs)
  #   imdbs.each do |imdb|
  #     score_cache(imdb)
  # end

  def scores_cache(imdbs)
    s_scores = score.where(imdb: imdbs)
    # s_scores = s_scores.to_a if imdbs.length != s_scores.length

    # s_scores.each(&:test_update)
    # s_scores = group_test(s_scores)
    # creates s_scores where they dont exist:
    new_scores =
      (imdbs - s_scores.map(&:imdb)).reduce([]) do |ary, imdb|
        ary.push(score.score_create(imdb))
      end
    Score.import(new_scores, validate: false, on_duplicate_key_ignore: true)
    s_scores + new_scores
  end

  # score a group of imdbs (e.g. rec_score)
  # confirm a show_score of 0 isnt because the show hasnt been scored at all.
  def avg_score(imdbs)
    score.select('sum(show_score) / count(show_score) as score')
         .where(imdb: imdbs)
         .where('cardinality(won_against) + cardinality(lost_against) > 0')[0]
         .score || -1
  end

  def rescore
    score.all.destroy_all
    # score_imdbs = whatsbetter.all.pluck(:first).uniq
    # rec_imdbs = Recommend.group_recommend(score_imdbs) - score_imdbs
    # check_scores(score_imdbs, rec_imdbs)
  end

  def people_score(person, job = nil)
    s_score = score_cache(person, job)
    s_score.show_score.to_i
  end

  def fill_scored_scores(imdbs)
    scored_imdbs = score.where(imdb: imdbs).where('show_score is not null')
                        .pluck(:imdb)
    scores_cache(imdbs - scored_imdbs).each do |s_score|
      s_score.show_score
      s_score.show_total
    end
  end

  def fill_rec_scores(imdbs)
    recs = Recommend.select('DISTINCT ON (second) second as imdb')
                    .where(first: imdbs).order(:second).map(&:imdb) + imdbs
    # recs += imdbs
    recs -= score.where(imdb: recs)
                 .where('rec_score is not null and rec_total is not null')
                 .pluck(:imdb)
    scores_cache(recs) do |r_score|
      # recs.uniq.each do |imdb|
      r_score.rec_score(false)
      r_score.rec_total
    end
  end

  def scored_imdbs
    score.where('show_total > 5')
         .where("imdb like 'tt%'")
         .where('show_score is not null')
         .order(show_score: :desc).pluck(:imdb)
  end

  def recommended_imdbs
    # .where('rec_total > 10')
    score.where('show_total < 6')
         .where('rec_score > 50').order(rec_score: :desc)
         .select { |s_score| s_score.rec_total >= 2 }.pluck(:imdb)
  end
end
