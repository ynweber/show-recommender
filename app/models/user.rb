# frozen_string_literal: true

# The User Model
class User < ApplicationRecord
  Array.include(MyExtensions::Enumerable::Frequency)
  validates :name, presence: true, uniqueness: true
  has_secure_password
  has_many :whatsbetter
  has_many :score

  include UserScore
  include UserShows

  def confusing_rankings
    # prob_imdbs = []
    prob_score = score.where(imdb: favorites).order(updated_at: :desc).find(&:confusing?)
    if prob_score
      prob_imdbs = prob_score.confusing
      # prob_imdbs += scores_cache(prob_imdbs).map(&:confusing).flatten
      Show.shows_by_imdb(prob_imdbs)
    else
      []
    end
    #
    # !! LEAVE THIS TILL IM SURE ABOVE WORKS !!
    #
    # prob_imdb = imdbs.find {|imdb| confusing?(imdb)}
    #   imdbs.find do |imdb|
    #     s_score = score.find_by(imdb: imdb)
    #     prob_imdbs = s_score.total_wins & s_score.total_losses
    #     if prob_imdbs.any?
    #       prob_imdbs += scores_cache(s_score.won_against)
    #                     .select { |w_score|
    #                     (w_score.won_against & prob_imdbs).any?
    #                     }.map(&:imdb)
    #     end
    #     prob_imdbs.any?
    #   end
    # if prob_imdbs.any?
    #   show = Show.show_by_imdb(prob_imdb)
    #   [show] + Show.shows_by_imdb(prob_imdbs).shuffle
    # else
    #   []
    # end
  end

  def fill_scores
    raw = 'select imdb from (select first as imdb from whatsbetters where '\
      "user_id = '#{id}' union all select second from whatsbetters where "\
      "user_id = '#{id}') i group by imdb having count(*) > 5"
    imdbs =
      ActiveRecord::Base.connection.execute(raw)
                        .map { |record| record['imdb'] }
    fill_scored_scores(imdbs)
    imdbs
  end

  # def fill_recs(imdbs)
  #   fill_rec_scores(imdbs)
  # end


  def favorites_check
    newest = whatsbetter.order(updated_at: :desc).limit(1).pluck(:updated_at)[0]
    newest ? newest > self[:updated_at] : true
  end

  def favorites_clear
    update!(
      favorites: nil,
      favorite_movies: nil,
      favorite_tv: nil
    )

    imdbs = ["ttmy_MOVIE#{id}", "ttmy_TV#{id}"]
    Show.where(imdb: imdbs).delete_all
    Recommend.where(first: imdbs).delete_all
    Score.where(imdb: imdbs).delete_all
  end

  def favorites
    self[:favorites] ||
      begin
        imdbs = fill_scores
        fill_rec_scores(imdbs)
        Show.fill_people(imdbs)
        update_and_return!(favorites: scored_imdbs)
      end
  end

  def self.scrape_reviewers(current_show)
    # ImdbService.reviewers simply returns a list users that have reviewed
    # the current show
    reviewers = ImdbService.reviewers(current_show.imdb)
    reviewers.each do |user_name|
      user = User.find_by(name: user_name + '.imdb')
      # create_reviewer builds reviewer scores
      user ||= create_reviewer(user_name)
      r_score = reviewer_good?(current_show, user.id)
      # return shows if r_score && shows.any?
      # shows = find_recs(current_show)
      # break if shows.length > 1
      break unless r_score
    end
    # []
    # shows = find_recs(current_show)
  end


  def self.reviewer_good?(show, user_id)
    r_score = Score.find_or_initialize_by(imdb: show.imdb, user_id: user_id)
    # not every user displays ratings
    show_score = r_score[:show_score]
    if show_score && show_score < 80
      ApplicationRecord.errors('no more users')
      false
    else
      true
    end
  end

  def self.create_reviewer(user_name)
    user = User.create_with(password: 'imdb')
               .find_or_create_by(name: user_name + '.imdb')
    ImdbService.build_reviewer(user)
    # checks if there are new whatsbetters
    user.fill_scores if user.favorites_check
    user
  end

  def self.user_ids_by_score(imdb)
    user_ids = Score.where(imdb: imdb).where("show_score >= #{min_score}")
                    .pluck(:user_id)
    return [] if user_ids.length < 10
    user_ids
  end

  def self.min_score(min_s = nil)
    @min_s = min_s ? min_s : @min_s || 90
  end

  # def self.min_reviewers(min_r = nil)
  #   @min_r = min_r ? min_r : @min_r || 10
  # end

  def self.user_mining(current_show)
    scrape_reviewers(current_show)
    find_recs(current_show)
  end
  #   update_recs(shows, current_show, 'users')
  #   shows = same_people(current_show, to_scrape)
  #   update_recs(shows, current_show, 'people')
  #   shows = Show.shows_by_imdb(current_show.back_recommend)
  #   update_recs(shows, current_show, 'back')
  #   if current_show.external_links.key?('MAL')
  #     shows = Show.shows_by_imdb(MalService.new(current_show).recommend)
  #     update_recs(shows, current_show, 'MAL')
  #   end

  def self.same_people(show)
    people = show.people.map { |person| person.split('_') }
    shows =
      people.map do |person|
        Show.shows_by_person(person[0], person[1], 3)
      end
    # shows.flatten.uniq.select { |r_show| show.relevant?(r_show) }
    shows.flatten.uniq.map(&:imdb)
  end

  def self.find_recs(current_show)
    user_ids = user_ids_by_score(current_show.imdb)
    imdbs = Score.select('imdb').where("show_score >= #{min_score}")
                 .where(user_id: user_ids).group(:imdb)
                 .having('count(imdb) > 4')
                 .order('count(imdb) desc').map(&:imdb)
    # imdbs = imdbs.select { |imdb| imdb.count > 4 }
    #   .sort_by(&:count).reverse.map(&:imdb)
    imdbs[0..50]
    # Show.shows_by_imdb(imdbs)[0..50]
    #   .select { |show| current_show.relevant?(show) }.map(&:imdb)
  end

  # what exactly was this supposed to do?
  def last_match
    last = whatsbetter.last
    # need to set up for new user
    # never rated anything, and tf last is nil!
    last ||= [Show.first.imdb, Show.last.imdb]
    Show.shows_by_imdb([last.first, last.second])
  end
end
