# frozen_string_literal: true

# the whatsbetter rating controller
class WhatsbetterController < ApplicationController
  include ApplicationHelper
  # def top_rated
  #   @shows_matchup = Whatsbetter.matchup_quick_list(Show.tv)
  #   respond_to do |format|
  #     format.html { render(template: 'whatsbetter/rate.html.erb') }
  #   end
  # end

  # def top_rated_movies
  #   # @shows = Show.where('subtext not like "%TV%"').order(popluarity: :desc)
  #   show =
  #     Show.movies.find do |movie|
  #       score = ScoreService.new(current_user, movie)
  #       score.show_total < 1
  #     end
  #   @shows_matchup = show.match if show
  #   render(template: 'whatsbetter/rate.html.erb')
  # end

  def rate_show
    show = Show.find_by(imdb: params['show'])
    @show = show
    @shows_matchup = [show, MatchService.new(show, current_user).match]
    render(template: 'whatsbetter/rate.html.erb', layout: false)
  end

  def rate_shows
    shows = Show.where(imdb: params['shows']) # .sort_by(&:popularity)
    imdbs = shows.map(&:imdb)
    imdbs -= current_user.score.skippable(imdbs)

    imdb = imdbs.sort_by {|imdb| current_user.score_cache(imdb).total_against.length}[0]
    show = Show.show_by_imdb(
      imdb
    )
    m_serv = MatchService.new(show, current_user)
    # show = Show.show_by_imdb(imdbs[0])
    #       .find_by do |f_show|
    #         s_score = f_show.score(current_user)
    #     !s_score.already_scored? || s_score.skipped_against.count < 3 ||
    #       Show.show?(f_show.imdb)
    #   end
    @shows_matchup = [show, m_serv.match] if show
    @show = show
    render(template: 'whatsbetter/rate.html.erb', layout: false)
  end

  def rate_show_recs
    show = Show.find_by(imdb: params['show'])
    shows = Show.shows_by_imdb(show.recommend).push(show).shuffle!
    @show = shows[0]
    render(template: 'whatsbetter/rate.html.erb')
  end

  def rate_group
    imdbs = params['shows']
    imdbs = imdbs.shuffle if imdbs
    @shows_matchup = Show.shows_by_imdb(find_match_from_group(imdbs)) if imdbs
    render(template: 'whatsbetter/rate.html.erb', layout: false)
  end

  def rate_confusing
    @shows = current_user.confusing_rankings[0..5]
    render(template: 'whatsbetter/_rate_multiple.html.erb', layout: false)
  end

  def find_match_from_group(imdbs)
    imdbs.each do |imdb_a|
      f_imdb_b = MatchService.new(imdb_a, current_user)
                             .find_match(imdbs - [imdb_a])[0]
      return [imdb_a, f_imdb_b] if f_imdb_b
    end
  end

  def first_run
    # if this is the first run for the app, not just the user,
    # there may not be any shows in the db
    # and this will break
    # do something about this !!
    genres = Show.select('distinct unnest(genres)').map(&:unnest)
    shows =
      ['TV', 'MOVIE'].product(genres).map do |show_type, genre|
        Show.where("'#{genre}' = ANY(genres)")
            .where(show_type: show_type).where('popularity is not null')
            .where("'Adult' != ANY(genres)")
            .order(popularity: :desc).limit(5)
      end
    @shows = shows.flatten.compact.select {|show| show.recommend.length > 1}

    render(template: 'whatsbetter/first_run.html.erb')
  end

  def create
    imdbs = params[:name].split(':')
    if imdbs.length == 2
      create_normal(find_first, find_second)
    else
      create_multiple
    end
    redirect_back(fallback_location: root_path)
  end

  def create_normal(first, second)
    Whatsbetter.destroy_whats(current_user.id, first, second)

    Whatsbetter.new(
      user: current_user,
      first: first,
      second: second,
      result: result
    ).save
    # redirect_to(:back)
  end

  def create_multiple
    imdbs = imdbs_split
    if result.zero?
      # !! untested
      imdbs.combination(2).each { |i_a, i_b| create_normal(i_a, i_b) }
    else
      winner = imdbs.delete_at(params[:score].to_i - 1)
      imdbs.each { |second| create_normal(winner, second) }
    end
  end

  def destroy
    Whatsbetter.destroy_whats(current_user.id, params[:first], params[:second])
    redirect_to(:back, notice: 'rating destroyed.')
  end

  def destroy_skips
    imdb = params[:imdb]
    skips = current_user.whatsbetter.skips(imdb)
    UserScore.clear_scores([imdb], current_user.id)
    # skips = current_user.whatsbetter.where(first: imdb, result: 0)
    # skips += current_user.whatsbetter.where(second: imdb, result: 0)
    skips.each(&:destroy)
    redirect_to(:back)
  end

  def update
    redirect_to(whatsbetter_path)
  end

  private

  def whatsbetter_params
    params.require(:name).permit(:score)
  end

  # def create_winner(imdbs, result)
  #   result = (1 unless result.zero?) || 0
  #   wb = Whatsbetter.new(
  #     user: self,
  #     first: imdbs[0],
  #     second: imdbs[1],
  #     result: result
  #   )
  #   wb.save
  # end

  def imdbs_split
    params[:name].split(':')
  end

  def find_first
    find_score == 1 ? imdbs_split[0] : imdbs_split[1]
  end

  def find_second
    find_score == 1 ? imdbs_split[1] : imdbs_split[0]
  end

  def find_score
    params[:score].keys.first.to_i
  end

  # this doesnt seem to be working!
  def result
    (1 unless find_score.zero?) || 0
  end
end
