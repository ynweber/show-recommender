# frozen_string_literal: true

# for recommendations
class RecommendsController < ApplicationController
  before_action :set_recommend, only: %i[show edit update destroy]

  # GET /recommends
  # GET /recommends.json
  def index
    @shows = get_recs('TV')
    @back_path = recommends_path
    respond_to do |format|
      format.html { render(template: 'shows/index') }
    end
  end

  def rec_check
    # Recommend.recs_check
    # imdbs = current_user.favorites

    rec_clear
    diffs = current_user.score.suprisings_rec_scores
    @notice = "#{diffs[0] / diffs[1].length} out of #{diffs[1].length}"
    @shows  = Show.shows_sorted_by_imdb(diffs[1][0..49])
    render(template: 'shows/index')
  end

  def rec_clear
    scores = current_user.score.where('show_total > 5').where("imdb like 'tt%'")
    scores.each do |score|
      score.update(
        show_total: nil, show_score: nil, won_against: nil, lost_against: nil,
        rec_total: nil, rec_score: nil
      )
      score.fill_score
    end
  end

  def rec_check_diff
    current_user.suprising_rec_scores
  end

  def rec_check_diff_old(imdbs)
    Show.shows_by_imdb(imdbs).reduce(0) do |sum, show|
      diff = rec_check_show_diff(
        show.score(current_user).rec_score,
        show.score.show_score
      )
      diff == Float::INFINITY || show.score.rec_score == -1 ? sum : sum + diff
    end
  end

  def rec_check_show_diff(rec_score, show_score)
    diff = ((show_score - rec_score) / Float(rec_score)) * 100
    diff == Float::INFINITY ? nil : diff
    # show_diff = show_score if show_diff == Float::INFINITY || show_diff.nan?
    # if rec_score > show_score
    #   (rec_score - show_score) / Float(show_score)
    # else
    #   (show_score - rec_score) / Float(rec_score)
    # end
  end

  # GET /recommends/1
  # GET /recommends/1.json
  def show; end

  # GET /recommends/new
  def new
    @recommend = Recommend.new
  end

  # GET /recommends/1/edit
  def edit; end

  # POST /recommends
  # POST /recommends.json
  def create
    recommend = Recommend.new(recommend_params)
    if recommend.save
      redirect_to(recommend, notice: 'Recommend created.')
    else
      render(:new)
    end
  end

  # PATCH/PUT /recommends/1
  # PATCH/PUT /recommends/1.json
  def update
    if recommend.update(recommend_params)
      redirect_to(recommend, notice: 'Recommend updated.')
    else
      render(:edit)
    end
  end

  # DELETE /recommends/1
  # DELETE /recommends/1.json
  def destroy
    recommend.destroy!
    respond_to do |format|
      format.html do
        redirect_to(
          recommends_url,
          notice: 'Recommend was successfully destroyed.'
        )
      end
    end
  end

  private

  def recommend
    @recommend ||= Recommend.find(params[:id])
  end

  def recommend_params
    params.require(:recommend).permit(:first, :second)
  end
end
