# frozen_string_literal: true

# keep track of users
class SessionsController < ApplicationController
  skip_before_action :require_login
  def new; end

  def create
    sess_params = params[:session]
    user = User.find_by(name: sess_params[:name])
    if user && user.authenticate(sess_params[:password])
      log_in(user)
      redirect_to(first_run_path)
      # Log the user in and redirect to the user's show page.
    else
      # Create an error message.
      render('new')
    end
  end

  def destroy
    log_out
    redirect_to(login_path)
  end
end
