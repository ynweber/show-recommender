# frozen_string_literal: true

# Show Controller
class ShowsController < ApplicationController
  # before_action :set_show, only: %i[show edit update destroy]
  before_action :set_user

  # GET /shows
  # GET /shows.json
  def index
    qury = params[:q]
    # ! why would i run shows_by_person (which expects an imdb)
    # ! when i search for something??
    shows ||= Show.shows_by_person(qury, nil, 3) + Show.shows_by_title(qury)
    shows = Show.shows_sorted_by_score(shows, current_user)
    @qury = qury
    @shows = shows.reverse
  end

  def imdb_search_title
    qury = params['q']
    shows = Show.shows_sorted_by_score(ImdbService.search(qury), current_user)
    #            .sort_by do |show|
    #   [show.score(current_user).rec_score, show.popularity]
    # end
    @qury = qury
    @shows = shows.reverse!
    render('/shows/index')
  end

  def imdb_cache
    @show = current_show
  end

  def all_genres; end

  def genre
    genre = params['genre']
    shows =
      Show.shows_by_genre(genre).sort_by do |show|
        [show.score(current_user).rec_score(false), show.popularity]
      end
    @qury = '&genres=' + genre
    @shows = shows[0..99]
    render('/shows/index')
  end

  def tv_tonight
    # tvshows = Show.where(first_aired: TvdbService.todays_date)
    # tvshows = TvdbService.new_today if tvshows.empty?
    # tvshow = Show.tv_tonight
    # tvshows.to_a
    start_date = params['start_date'] || ApplicationRecord.todays_date
    end_date = params['end_date'] || ApplicationRecord.todays_date
    shows =
      Show.tv_tonight(start_date, end_date).sort_by do |show|
        [current_user.score_cache(show.imdb).rec_score, show.popularity]
      end
    @shows = shows.reverse
    @heading = "TV For #{start_date}-#{end_date}"
    render('/shows/index')
  end

  def full_cast
    shows = current_show.full_cast
    @shows = shows
    render('/shows/index')
  end

  def dvd
    @shows = ImdbService.dvd
    respond_to do |format|
      format.html { render('/shows/index') }
    end
  end

  def show_why
    @show = Show.show_by_imdb(params['show'])
  end

  # GET /shows/1
  # GET /shows/1.json
  def show
    if Show.person?(current_show.imdb)
      limit = params[:limit] ? 100 : 3
      shows = Show.shows_by_person(current_show.imdb, nil, limit)
      @shows = Show.shows_sorted_by_score(shows, current_user).reverse
      @show = current_show
      render('/shows/index')
    else
      @show = current_show
    end
  end

  # GET /shows/new
  def new
    @show = Show.new
  end

  # def scrape
  #   Scraper.scrape
  # end

  # GET /shows/1/edit
  def edit; end

  def user_mining
    # scrape = current_user.id == 3 ? true : false
    # scrape_reviewers(current_show) if current_user.id == 3
    current_show.expand_recs
    # User.user_mining(current_show)
    # @show = current_show
    redirect_to(current_show)
  end

  # POST /shows
  # POST /shows.json
  def create
    show = Show.new(imdb: show_params[:imdb])
    show.save!
    respond_to do |format|
      format.html { redirect_to(shows_url, notice: 'Show created.') }
    end
  end

  # PATCH/PUT /shows/1
  # PATCH/PUT /shows/1.json
  def update
    # respond_to do |format|
    if current_show.update(show_params)
      redirect_to(current_show, notice: 'Show updated.')
      # format.json { render :show, status: :ok, location: @show }
    else
      render(:edit)
      # format.json { render json: @show.errors, status: :unprocessable_entity }
    end
  end

  # DELETE /shows/1
  # DELETE /shows/1.json
  def destroy
    current_show.destroy!
    respond_to do |format|
      format.html { redirect_to(shows_url, notice: 'Show destroyed.') }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def current_show
    # need a check for bogus show
    # imdb = params[:id]
    @current_show = Show.show_by_imdb(params[:id])
    # @show ||= Show.new(imdb: imdb)
  end

  def set_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def show_params
    params.require(:show).permit(:name, :imdb)
  end
end
