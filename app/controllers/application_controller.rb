# frozen_string_literal: true

# app global controller
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :require_login, except: %i[new create]
  include SessionsHelper

  private

  def require_login
    redirect_to(login_url) unless current_user
  end
end
