# frozen_string_literal: true

# User Controller
class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    user = User.new(user_params)
    if user.save
      redirect_to(login_path)
    else
      render('new')
    end
  end

  def my_movie
    show = current_user.my_movie
    @show = show
    @rate_path = rate_group_path(shows: show.score.lost_against)
    render('/shows/show')
  end

  def my_tv
    show = current_user.my_tv
    @show = show
    @rate_path = rate_group_path(shows: show.score.lost_against)
    render('/shows/show')
  end

  private

  def user_params
    params.require(:user).permit(
      :name, :email, :password,
      :password_confirmation
    )
  end
end
