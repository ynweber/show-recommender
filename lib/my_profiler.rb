class MyProfiler
  def self.m_hash(mthd, event_type)
    @m_hash ||= Hash.new { |hsh, ky| hsh[ky] = [] }
    if event_type == 'call'
      @m_hash[mthd].push([Time.now])
    else
      @m_hash[mthd][-1].push(Time.now)
    end
  end

  def self.print_report(page)
    return unless @m_hash
    to_print = []
    @m_hash.keys.each do |mthd|
      m_total =
        @m_hash[mthd].reduce(0) do |total, occurence|
          if occurence[1]
            total + (occurence[1] - occurence[0])
          else
            puts(mthd)
            total
          end
        end
      to_print.push(
        [
          m_total,
          @m_hash[mthd].count,
          @m_hash[mthd][0],
          mthd
        ]
      )
    end

    # to_print.sort.each do |mthd_array|
    #   puts mthd_array
    #   puts ''
    # end

    f_name = page.split('__')[0]
    to_print = to_print.sort.map { |ln| ln.join("\t") }.join("\n")
    File.open("#{Rails.root}/tmp/profiling/#{f_name}.txt", 'w') do |file|
      file.write(to_print.force_encoding('UTF-8'))
    end
    @m_hash = Hash.new { |hsh, ky| hsh[ky] = [] }
  end
end
