# README
ruby 2.4.1
rails 5.0.2

Show Recommender

This is an older, nearly finished program. I'm in the process of updating it.
Maybe you remember 'whatsbetter.com' The idea was it would show you two pictures. e.g. Hilary Clinton vs. Donald Trump. Puppys vs. Kittens. You picked which you thought was better. And it would show you how your results compared to others.

Show Recommender works in a similar fashion. It shows you two TV Shows, or two Movies. You pick which you like better (or skip). After a few ratings the program learns your preferences, and finds recommendations.

To show these recommendations it creates a fictional show called 'MyTV' or 'MyMovie'. Starring your favorite actors, created by your favorite creators, etc. That show recommends to you other shows with an estimated rating.

This is an oversimplified description of how it works, but it does work, and if you happen to be looking for a new tv show or movie, this could be useful.

A word of warning. it scapes imdb (and others) for show it is unfamiliar with. this can be slow. 
